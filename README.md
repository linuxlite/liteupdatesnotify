Lite Updates Notify
================

Configure updates notification frequency in Linux Lite.

This application is a work-in-progress and is far from complete.

![](https://imgur.com/uBaSzUJ.png)

![](https://imgur.com/mNeAAqR.png)

## License ![License](https://img.shields.io/badge/license-GPLv2-green.svg)

This project is under the GPLv2 license. Unless otherwise stated in individual files.

## Authors
- [Jerry Bezencon](https://github.com/linuxlite/)
- [Ralphy](https://github.com/ralphys)
